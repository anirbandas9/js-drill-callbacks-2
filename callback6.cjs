const fs = require('fs');
const getBoardInformation = require('./callback1.cjs');
const getDataFromLists = require('./callback2.cjs');
const getCardInformation = require('./callback3.cjs');

const timeDelay = Math.floor(Math.random() * 2000);

function getThanosData3(boardName) {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) throw err;
            const boardID = JSON.parse(data).find(data => data.name === boardName).id;
            getBoardInformation(boardID, (err, thanosBoard) => {
                if (err) throw err;
                console.log('Thanos Boards:', thanosBoard);
                getDataFromLists(boardID, (err, thanosLists) => {
                    if (err) throw err;
                    console.log('Thanos Lists: ', thanosLists);
                    thanosLists.forEach(list => {
                        const id = list.id;
                        getCardInformation(id, (err, cardData) => {
                            if (err) throw err;
                            console.log(`${list.name} Cards:`, cardData);
                        })
                    })
                })
            })
        })
    }, timeDelay);
}

module.exports = getThanosData3;