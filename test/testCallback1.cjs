const getBoardInformation = require('../callback1.cjs');

const boardId = "mcu453ed";

getBoardInformation(boardId, (err, boardData) => {
    if (err) {
        throw err;
    } else {
        console.log(boardData);
    }
});