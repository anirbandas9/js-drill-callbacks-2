const fs = require('fs');
const getBoardInformation = require('./callback1.cjs');
const getDataFromLists = require('./callback2.cjs');
const getCardInformation = require('./callback3.cjs');

const timeDelay = Math.floor(Math.random() * 2000);

function getThanosData2(boardName, listNames) {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) throw err;
            const boardID = JSON.parse(data).find(data => data.name === boardName).id;
            getBoardInformation(boardID, (err, thanosBoard) => {
                if (err) throw err;
                console.log('Thanos Board:', thanosBoard);
                getDataFromLists(boardID, (err, thanosLists) => {
                    if (err) throw err;
                    console.log('Thanos Lists:', thanosLists);
                    listNames.forEach((curr) => {
                        const listId = thanosLists.find(list => list.name === curr).id;
                        getCardInformation(listId, (err, cards) => {
                            if (err) throw err;
                            console.log(`${curr} Cards:`, cards);
                        })
                    });
                })
            })
        })
    }, timeDelay);
}

module.exports = getThanosData2;