const fs = require('fs');

const timeDelay = Math.floor(Math.random() * 2000);

function getDataFromLists(boardID, cb) {
    setTimeout(() => {
        fs.readFile('lists.json', 'utf-8', (err, lists) => {
            if (err) cb(err);
            const list = JSON.parse(lists)[boardID];
            cb(null, list);
        });
    }, timeDelay);
}

module.exports = getDataFromLists;
