const fs = require("fs");

const timeDelay = Math.floor(Math.random() * 2000);

function getBoardInformation(boardID, cb) {
    setTimeout(() => {
        fs.readFile('boards.json', 'utf-8', (err, data) => {
            if (err) {
                cb(err);
            } else {
                const boards = JSON.parse(data);
                const board = boards.find((board) => board.id === boardID);
                cb(null, board);
            }
        });
    }, timeDelay);
}

module.exports = getBoardInformation;