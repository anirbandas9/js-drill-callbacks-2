const fs = require('fs');

const timeDelay = Math.floor(Math.random() * 2000);

function getCardInformation(listID, cb) {
    setTimeout(() => {
        fs.readFile('cards.json', 'utf8', (err, cards) => {
            if (err) cb(err);
            const card = JSON.parse(cards)[listID];
            if (card !== undefined) cb(null, card);
        })
    }, timeDelay);
}

module.exports = getCardInformation;
